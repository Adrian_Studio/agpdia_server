function codify(id) {
  return "a" + id + "c";
}

function decodify(id) {
  return id.substring(1, id.length - 1);
}

module.exports = { codify, decodify };
