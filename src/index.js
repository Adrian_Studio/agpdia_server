const { response } = require("express");
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const cron = require('node-cron');

const {updateAllMessages} = require("./helpers/messagesManagement")
const { periodicInputs} = require("./helpers/autoInputShowData")

const config = require("./config/config");

const app = express();

// Initiaizations
require("./database");

// Settings
app.set("json spaces", 2);

// Middlewares
app.use(morgan('remote address: :remote-addr; date: :date[clf]; method: :method; url: :url; HTTP version: HTTP/:http-version; status: :status; response time: :response-time ms; length: :res[content-length]'));
// app.use(morgan('dev'));
app.use(express.urlencoded({limit: '50mb', extended: true}));
app.use(express.json({limit: '50mb'}));
app.use(cors())

// Configurar cabeceras y cors
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

// Routes
app.use("/api", require("./routes"));

// Schedule tasks to be run on the server.
cron.schedule('0 0 * * *', function() {
  updateAllMessages()
  // periodicInputs()
});

// Run of cron funtions at start
updateAllMessages()

// Server is
app.listen(config.port, () => {
  console.log(`Server on port ${config.port}`);
});

