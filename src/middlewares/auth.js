const manageToken = require("../services/manage_token");

function isAuth(req, res, next) {
  if (!req.headers.authorization)
    return res.status(403).json({ message: "You are not authorized" });

  const token = req.headers.authorization.split(" ")[1];

  manageToken
    .decodeToken(token)
     .then((response) => {
       req.userId = response.userId;
       req.userLanguage = response.userLanguage;
       next();
     })
     .catch((response) => {
       res.status(response.status).json(response.message);
     });
}

module.exports = isAuth;
