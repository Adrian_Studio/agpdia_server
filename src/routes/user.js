const express = require("express");
const router = express.Router();
const isAuth = require("../middlewares/auth");

const service = require("../services/manage_token");

const User = require("../models/User");

const COLLECTION = "User(s)";

// SignUp
router.post("/signup", function (req, res) {
  const { email, display_name, password } = req.body;

  const newUser = new User({ email, display_name, password });

  newUser.save((err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
      });

    return res.status(200).json({
      message: "The user has been registered suscessfully",
      token: service.createToken(result),
      language: result.language,
      display_name: result.display_name,
      avatar: result.avatar,
    });
  });
});

// SignIn
router.post("/signin", function (req, res) {
  const { email, password } = req.body;

  User.findOne({ email }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    result.comparePassword(password, function (err, isMatch) {
      if (err)
        return res.status(500).json({ message: `Error comparing passwords` });
      if (!isMatch)
        return res.status(400).json({
          message: `The pasword doen't match`,
        });

      User.findByIdAndUpdate(
        result._id,
        { last_login: Date.now() },
        (err, result) => {
          //For some reason it doesn't work if there is not (err, result)=>{}
        }
      );
      return res.status(200).json({
        message: "The user has been logged suscessfully",
        token: service.createToken(result),
        language: result.language,
        display_name: result.display_name,
        avatar: result.avatar,
      });
    });
  });
});

// Change Password
router.post("/new_password", isAuth, function (req, res) {
  const user_id = req.userId;
  const { password } = req.body;

  // Need to be finding first and then saving so the .pre works and hash the password
  User.findById(user_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to update the Password in the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The User doesn't exist??` });

    result.password = password;
    result.save((err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
        });
      return res.status(200).json({ message: `Password updated successfully` });
    });
  });
});

// Change Language
router.post("/new_language", isAuth, function (req, res) {
  const user_id = req.userId;
  const { language } = req.body;

  User.findById(user_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to find the User in the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The User doesn't exist??` });

    return res.status(200).json(result);
  });
});

// Change Displayed Name
router.post("/new_name", isAuth, function (req, res) {
  const user_id = req.userId;
  const { display_name } = req.body;

  User.findByIdAndUpdate(
    user_id,
    { display_name },
    { runValidators: true },
    (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to update the Name in the db: ${err}`,
        });
      if (!result)
        return res.status(404).json({ message: `The User doesn't exist??` });

      return res.status(200).json(result);
    }
  );
});

module.exports = router;
