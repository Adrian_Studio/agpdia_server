const express = require("express");
const router = express.Router();

const Message = require("../../models/alerts/Message");

const COLLECTION = "Message(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  Message.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });
      
    return res.status(200).json(result);
  });
});


// Update all documents
router.put("/", async function (req, res) {
  const user_id = req.userId;
  await Message.updateMany({user_id}, { unread: false });
});



//Add and Delete messages is done in the background, can be found in helpers and is call by cron

module.exports = router;
