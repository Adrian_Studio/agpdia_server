const express = require("express");
const router = express.Router();

const AccountsAlert = require("../../models/alerts/AccountsAlert");
const Item = require("../../models/inventories/Item");
const Concept = require("../../models/accounts/Concept");
const Record = require("../../models/accounts/Record");
const Message = require("../../models/alerts/Message");
const {updateAlertMessages} = require("../../helpers/messagesManagement")
const COLLECTION = "AccountsAlert(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;
  AccountsAlert.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });
        
    

    return res.status(200).json( result );
  });
});

// Get a document
router.get("/:id", function (req, res) {
  const { id } = req.params;

  AccountsAlert.findById(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
});

// Add a document
router.post("/", async function (req, res) {
  const user_id = req.userId;
  const { name, concept_id, periodicity_months, periods_to_alert, start_date, end_date,note, is_active} = req.body;

  await Concept.findById(concept_id, (err, result) => {
    if (err)
    return res.status(400).json({
      message: `Error trying to get the Concept from the db: ${err}`,
    });
    
    if (!result)
    return res.status(404).json({ message: `The Concept doesn't exist` });
    
    concept_name = result.name;
    concept_group = result.group;
    concept_subgroup = result.subgroup;
    category_id = result.category_id;
    category_name = result.category_name;
  });
  
  const newAccountsAlert = new AccountsAlert({ user_id, name, concept_id, periodicity_months, periods_to_alert, start_date, end_date,note, is_active, concept_name, category_id, category_name });
  
  newAccountsAlert.save((err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
      });

    result.user_id = undefined;

    updateAlertMessages(result._id)
    
    return res.status(200).json( result );
  });
});

// Update a document
router.put("/:id", function (req, res) {
  const { id } = req.params;
  const updateData = req.body;

  AccountsAlert.findByIdAndUpdate(
    id,
    updateData,
    { runValidators: true },
    (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
        });

      if (!result)
        return res
          .status(404)
          .json({ message: `The ${COLLECTION} doesn't exist` });

      Item.updateMany(
        { group_id: id },
        { group_name: updateData.name },
        (err, result) => {
          if (err)
            return res.status(400).json({
              message: `Error updating Items: ${err}`,
            });
          // return res.status(400).json({
          //     message: `The item has been updated successfully`,
          //   });
        }
      );

      updateAlertMessages(id)

      return res.status(200).json(result);
    }
  );
});

// Delete a document
router.delete("/:id", function (req, res) {
  const { id } = req.params;
  console.log('faltará eliminar los mensajes para esta alerta')
  // Item.exists({ group_id: id }, (err, result) => {
  //   if (err)
  //     return res.status(400).json({
  //       message: `Error trying checking if it's: ${err}`,
  //     });
  //   if (result)
  //     return res.status(400).json({
  //       message: `The alert it's used`,
  //     });
    AccountsAlert.findByIdAndDelete(id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
        });

      if (!result)
        return res.status(400).json({
          message: `The ${COLLECTION} doesn't exist`,
        });


        updateAlertMessages(id)

      return res
        .status(200)
        .json({ message: `The ${COLLECTION} has been deleted` });
    });
  // });
});

module.exports = router;
