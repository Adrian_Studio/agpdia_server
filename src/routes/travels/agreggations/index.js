const express = require("express");
const router = express.Router();

router.use("/travels", require("./travel"));

module.exports = router;
