const express = require("express");
const router = express.Router();

router.use("/travels", require("./travel"));
router.use("/aggs", require("./agreggations"));

module.exports = router;
