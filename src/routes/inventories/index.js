const express = require("express");
const router = express.Router();

router.use("/items", require("./item"));
router.use("/locations", require("./location"));
router.use("/groups", require("./group"));
router.use("/inventories", require("./inventory"));
router.use("/aggs", require("./agreggations"));

module.exports = router;
