const express = require("express");
const router = express.Router();

router.use("/locations", require("./location"));
router.use("/items", require("./item"));

module.exports = router;
