const express = require("express");
const router = express.Router();

const Item = require("../../../models/inventories/Item");
const { decodeToken } = require("../../../services/manage_token");
const diacriticSensitiveRegex = require("../../../helpers/diacriticSensitiveRegex");

const COLLECTION = "Item(s)";

// Get the list of documents of collection
router.post("/list", async (req, res) => {
  const user_id = req.userId;
  const { locations, groups, state, note, limit } = req.body;
  filter = {
    user_id: user_id,
  };

  if (locations) {
    filter.location_id = {};
    filter.location_id.$in = locations;
  }
  if (groups) {
    filter.group_id = {};
    filter.group_id.$in = groups;
  }

  if (state){
    filter.state = {};
    filter.state.$in = state;
  }

  if (note) {
    if (!filter.$or) filter.$or = [];
    filter.$or.push({
      note: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      group_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      location_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      inventory_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      acquisition_place: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      brand: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      identifier: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
  }

  try {
    var recordsList = await Item.aggregate([
      {
        $match: filter,
      },
      { $addFields: { disposed: { $type: "$dispose_date" } } },
      { $sort: { disposed: -1, dispose_date: -1 ,acquisition_date: -1 } },
      { $limit: limit ? limit : Infinity },
      {
        $group: {
          _id: null,
          items: {
            $push: {
              id: "$_id",
              name: "$name",
              acquisition_date: "$acquisition_date" ,
              dispose_date:"$dispose_date" ,
              inventory: "$inventory_name",
              location: "$location_name",
              group: "$group_name",
              brand: "$brand",
              identifier: "$identifier",
              note: "$note",
              state: "$state",
              acquisition_price: "$acquisition_price",
              acquisition_place: "$acquisition_place",
              currency_symbol: "$currency_symbol",
              currency_format: "$currency_format",
              has_main_currency: "$has_main_currency",
              image:"$image",
              invoice:"$invoice",
            },
          },
        },
      },
      // {
      //   $addFields: {
      //     date: "$_id",
      //   },
      // },
      // {
      //   $group: {
      //     _id: null,
      //     total: { $sum: "$dailyTotal" },
      //     daily: {
      //       $push: {
      //         id: "$_id",
      //         dailyTotal: "$dailyTotal",
      //         records: "$records",
      //         date: "$date",
      //       },
      //     },
      //   },
      // },
    ]);

    if (!recordsList.length)
      return res
        .status(404)
        .json({ message: "There is no data with this filter conditions" });

    return res.status(200).json(recordsList[0]);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the list: " + err });
  }
});

module.exports = router;
