const express = require("express");
const router = express.Router();

const House = require("../models/House");
const Record = require("../models/accounts/Record");

const COLLECTION = "House(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  House.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
});

// Get a document
router.get("/:id", function (req, res) {
  const { id } = req.params;

  House.findById(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
});

// Add a document
router.post("/", function (req, res) {
  const user_id = req.userId;
  const { name } = req.body;

  const newHouse = new House({ user_id, name });

  newHouse.save((err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
      });

    result.user_id = undefined;
    return res.status(200).json( result );
  });
});

// Update a document
router.put("/:id", function (req, res) {
  const { id } = req.params;
  const updateData = req.body;

  House.findByIdAndUpdate(
    id,
    updateData,
    { runValidators: true },
    (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
        });

      if (!result)
        return res
          .status(404)
          .json({ message: `The ${COLLECTION} doesn't exist` });

      Record.updateMany(
        { house_id: id },
        { house_name: updateData.name },
        (err, result) => {
          if (err)
            return res.status(400).json({
              message: `Error updating Records: ${err}`,
            });
          // return res.status(400).json({
          //     message: `The record has been updated successfully`,
          //   });
        }
      );

      return res.status(200).json(result);
    }
  );
});

// Delete a document
router.delete("/:id", function (req, res) {
  const { id } = req.params;

  Record.exists({ house_id: id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying checking if it's: ${err}`,
      });
    if (result)
      return res.status(400).json({
        message: `The House it's used`,
      });

    House.findByIdAndDelete(id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
        });

      if (!result)
        return res.status(400).json({
          message: `The ${COLLECTION} doesn't exist`,
        });

      return res
        .status(200)
        .json({ message: `The ${COLLECTION} has been deleted` });
    });
  });
});

module.exports = router;
