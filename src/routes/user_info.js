const express = require("express");
const router = express.Router();
const isAuth = require("../middlewares/auth");

const User = require("../models/User");
const Currency = require("../models/Currency");


// Check if its user
router.post("/is_user", isAuth, function (req, res) {
  const user_id = req.userId;

  User.findById(user_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the User from the db: ${err}`,
      });
    if (!result)
      return res.status(404).json({ message: `The User doesn't exist??` });

    return res.status(200).json({ isUser: true });
  });
});

// Get the main currency
router.get("/main_currency", isAuth, function (req, res) {
  const user_id = req.userId;

  Currency.findOne({user_id: user_id, is_main: true}, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Currency from the db: ${err}`,
      });
    if (!result)
      return res.status(404).json({ message: `The User doesn't exist??` });

    return res.status(200).json( result );
  });
});

module.exports = router;
