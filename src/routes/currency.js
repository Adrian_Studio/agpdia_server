const express = require("express");
const router = express.Router();

const Currency = require("../models/Currency");
const CurrencyInfo = require("../models/CurrencyInfo");
const Account = require("../models/accounts/Account");
const Record = require("../models/accounts/Record");
const Item = require("../models/inventories/Item");

const COLLECTION = "Currency(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  Currency.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
});

// Get a document
router.get("/:id", function (req, res) {
  const { id } = req.params;

  Currency.findById(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
});

// Add a document
router.post("/", function (req, res) {
  const user_id = req.userId;
  const { code, format, is_main, exchange } = req.body;

  CurrencyInfo.findById(code, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Category from the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The Category doesn't exist` });

    const symbol = result.symbol;

    const newCurrency = new Currency({
      user_id,
      code,
      format,
      symbol,
      is_main,
      exchange,
    });

    newCurrency.save((err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
        });

      result.user_id = undefined;
      return res.status(200).json( result );
    });
  });
});

// Update a document
router.put("/:id", function (req, res) {
  const { id } = req.params;
  const updateData = req.body;
  const { code } = req.body;

  CurrencyInfo.findById(code, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Category from the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The Category doesn't exist` });

    updateData.symbol = result.symbol;

    Currency.findByIdAndUpdate(
      id,
      updateData,
      { runValidators: true },
      (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
          });

        if (!result)
          return res
            .status(404)
            .json({ message: `The ${COLLECTION} doesn't exist` });

        Account.updateMany(
          { currency_id: id },
          { symbol: updateData.symbol },
          { format: updateData.format },
          { has_main_currency: updateData.is_main },
          (err, result) => {
            if (err)
              return res.status(400).json({
                message: `Error updating Account: ${err}`,
              });
            // return res.status(400).json({
            //     message: `The Account has been updated successfully`,
            //   });
            Record.updateMany(
              { currency_id: id },
              { currency_symbol: updateData.symbol },
              { currency_format: updateData.format },
              { has_main_currency: updateData.is_main },
              (err, result) => {
                if (err)
                  return res.status(400).json({
                    message: `Error updating Record: ${err}`,
                  });
                // return res.status(400).json({
                //     message: `The Record has been updated successfully`,
                //   });
              }
            );
          }
        );

        Item.updateMany(
          { currency_id: id },
          { currency_symbol: updateData.symbol },
          { currency_format: updateData.format },
          { has_main_currency: updateData.is_main },
          (err, result) => {
            if (err)
              return res.status(400).json({
                message: `Error updating Item: ${err}`,
              });
            // return res.status(400).json({
            //     message: `The Item has been updated successfully`,
            //   });
          }
        );

        return res.status(200).json(result);
      }
    );
  });
});

// Delete a document
router.delete("/:id", function (req, res) {
  const { id } = req.params;
  Record.exists({ currency_id: id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying checking if it's: ${err}`,
      });
    if (result)
      return res.status(400).json({
        message: `The Currency it's used`,
      });

    Account.exists({ currency_id: id }, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying checking if it's: ${err}`,
        });
      if (result)
        return res.status(400).json({
          message: `The Currency it's used`,
        });

      Item.exists({ currency_id: id }, (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error trying checking if it's: ${err}`,
          });
        if (result)
          return res.status(400).json({
            message: `The Currency it's used`,
          });

        Currency.findByIdAndDelete(id, (err, result) => {
          if (err)
            return res.status(400).json({
              message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
            });

          if (!result)
            return res.status(400).json({
              message: `The ${COLLECTION} doesn't exist`,
            });

          return res
            .status(200)
            .json({ message: `The ${COLLECTION} has been deleted` });
        });
      });
    });
  });
});

module.exports = router;
