const express = require("express");
const router = express.Router();

const Account = require("../../models/accounts/Account");
const Currency = require("../../models/Currency");
const Record = require("../../models/accounts/Record");

const COLLECTION = "Account(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  Account.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json(result);
  }).sort({ name: 1 });
});

// Get a document
router.get("/:id", function (req, res) {
  const { id } = req.params;

  Account.findById(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json(result);
  });
});

// Add a document
router.post("/", function (req, res) {
  const user_id = req.userId;
  const { currency_id, name, incial_amount, image, color, statistics } = req.body;

  Currency.findById(currency_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Currency from the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The Currency doesn't exist` });

    const code = result.code;
    const symbol = result.symbol;
    const format = result.format;
    const has_main_currency = result.is_main;

    const newAccount = new Account({
      user_id,
      currency_id,
      name,
      incial_amount,
      code,
      symbol,
      format,
      has_main_currency,
      image,
      color,
      statistics
    });

    newAccount.save((err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
        });

      result.user_id = undefined;
      return res.status(200).json(result);
    });
  });
});

// Update a document
router.put("/:id", async function (req, res) {
  const { id } = req.params;
  const updateData = req.body;
  const { currency_id } = req.body;

  try {
    await Currency.findById(currency_id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to get the Currency from the db: ${err}`,
        });

      if (!result)
        return res.status(404).json({ message: `The Currency doesn't exist` });

      updateData.code = result.code;
      updateData.symbol = result.symbol;
      updateData.format = result.format;
      updateData.has_main_currency = result.is_main;
    });

    await Account.findByIdAndUpdate(
      id,
      updateData,
      { runValidators: true },
      (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
          });

        if (!result)
          return res
            .status(404)
            .json({ message: `The ${COLLECTION} doesn't exist` });
      }
    );

    await Record.updateMany(
      { account_id: id },
      {
        account_name: updateData.name,
        account_color: updateData.color,
        currency_id: updateData.currency_id,
        currency_symbol: updateData.symbol,
        currency_format: updateData.format,
        has_main_currency: updateData.has_main_currency,
      },
      (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error updating Records: ${err}`,
          });
        // return res.status(400).json({
        //     message: `The Record has been updated successfully`,
        //   });
      }
    );

    return res.status(200).json(updateData);
  } catch (err) {}
});

// Delete a document
router.delete("/:id", function (req, res) {
  const { id } = req.params;

  Record.exists({ account_id: id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying checking if it's: ${err}`,
      });
    if (result)
      return res.status(400).json({
        message: `The Account it's used`,
      });

    Account.findByIdAndDelete(id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
        });

      if (!result)
        return res.status(400).json({
          message: `The ${COLLECTION} doesn't exist`,
        });

      return res
        .status(200)
        .json({ message: `The ${COLLECTION} has been deleted` });
    });
  });
});

module.exports = router;
