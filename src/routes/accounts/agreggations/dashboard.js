const express = require("express");
const router = express.Router();
const moment = require("moment");

const Account = require("../../../models/accounts/Account");
const Record = require("../../../models/accounts/Record");
const { decodeToken } = require("../../../services/manage_token");
const diacriticSensitiveRegex = require("../../../helpers/diacriticSensitiveRegex");
const currencyFormater = require("../../../helpers/currencyFormater");

const mongoose = require("mongoose");
const e = require("express");
const { response } = require("express");
const ObjectId = mongoose.Types.ObjectId;
const COLLECTION = "Account(s)";

router.post("/dashboardAccounts", async (req, res) => {
  const user_id = req.userId;

  var filter = {
    user_id: user_id,
  };

  try {
    var accountsInitialAmount = await Account.aggregate([
      {
        $match: filter,
      },
      { $sort: { name: 1 } },
    ]);

    var balanceShown = await Record.aggregate([
      {
        $match: filter,
      },
      {
        $project: {
          date: {
            $dateToString: {
              format: "%d-%m-%Y",
              date: "$date",
              timezone: "+02",
            },
          },
          day: { $dayOfMonth: { date: "$date", timezone: "+02" } },
          month: { $month: { date: "$date", timezone: "+02" } },
          year: { $year: { date: "$date", timezone: "+02" } },
          account_id: "$account_id",
          amount: "$amount",
        },
      },
      {
        $group: {
          _id: { year: "$year", month: "$month", account_id: "$account_id" },
          amount: { $sum: "$amount" },
        },
      },
      { $sort: { _id: 1 } },
    ]);

    var response = {};
    var excludedAccounts = [];
    if (accountsInitialAmount[0]) {
      response["total"] = {
        label: "Total",
        data: [],
        date: [],
        color: "#a1ae2590",
        format: accountsInitialAmount[0].format,
        symbol: accountsInitialAmount[0].symbol,
        available: false,
      };
      response["total_disponible"] = {
        label: "Disponible",
        data: [],
        date: [],
        color: "#a1ae25",
        format: accountsInitialAmount[0].format,
        symbol: accountsInitialAmount[0].symbol,
        available: true,
      };
    }
    var total = { total: 0, total_disponible: 0 };
    accountsInitialAmount.forEach((account) => {
      var available = true
      if (["stock"].includes(account.statistics)) {
        excludedAccounts.push(account._id.toString());
        total["total_disponible"] -= account.initial_amount;
        available = false
      }
      response[account._id] = {
        label: account.name,
        data: [],
        date: [],
        color: account.color,
        format: account.format,
        symbol: account.symbol,
        available: available
      };
      total[account._id] = account.initial_amount;
      total["total"] += account.initial_amount;
      if (!excludedAccounts.includes(account._id.toString())) {
        total["total_disponible"] += account.initial_amount;
      }
    });
    // console.log(total)
    // response["total"].data.push(total["total"]);
    // response["total_disponible"].data.push(total["total_disponible"]);
    if (balanceShown[0]) {
      var previous_date = moment(
        new Date(
          balanceShown[0]._id.year.toString() +
            "-" +
            balanceShown[0]._id.month.toString() +
            "-01"
        )
      ).subtract(1, "month");
      // Object.keys(response).forEach((account_id) => {
      //   response[account_id].date.push(moment(new Date(balanceShown[0]._id.year.toString() +'-'+balanceShown[0]._id.month.toString() +'-01')).subtract(1,"month").format('MM/YYYY'))
      // });
      arrays = { dates: [], dates_formated:[], total: [], total_disponible: [] };
      while (previous_date <= moment()) {
        arrays.dates.push(new Date(previous_date));
        arrays.dates_formated.push(previous_date.format('MM-YYYY'));
        arrays.total.push(null);
        arrays.total_disponible.push(null);
        accountsInitialAmount.forEach((account) => {
          if (!arrays[account._id]) arrays[account._id] = [];
          arrays[account._id].push(null);
        });
        previous_date = previous_date.add(1, "month");
      }

      balanceShown.forEach((entry) => {
        var date = moment(new Date(
          entry._id.year.toString() + "-" + entry._id.month.toString() + "-01"
        )).format('MM-YYYY');
        var index = arrays.dates_formated.indexOf(date);
  
        total[entry._id.account_id] += entry.amount;
        arrays[entry._id.account_id][index] = total[entry._id.account_id];
        
        total["total"] += entry.amount;
        arrays["total"][index] = total["total"];
        

        if (!excludedAccounts.includes(entry._id.account_id)) {
          total["total_disponible"] += entry.amount;
        }
        arrays["total_disponible"][index] = total["total_disponible"];
      });
      Object.keys(arrays).forEach(key => {
        for (let index = 1; index < arrays[key].length; index++) {
          if(!arrays[key][index]){arrays[key][index] = arrays[key][index-1]}
        }
        
      });
      Object.keys(response).forEach((account_id) => {
        response[account_id].date = arrays['dates_formated']
        response[account_id].data = arrays[account_id]
        response[account_id].ultimate = total[account_id];
        response[account_id].total_percentage = (total[account_id] / total["total"] * 100)
        response[account_id].total_available_percentage = (total[account_id] / total["total_disponible"] * 100)
        response[account_id].variation_penultimate_month = response[account_id].data.length > 1
          ? response[account_id].data[response[account_id].data.length - 2] - response[account_id].data[response[account_id].data.length - 3]
          : 0;
          response[account_id].variation_antepenultimate_month = response[account_id].data.length > 2
          ? response[account_id].data[response[account_id].data.length - 3] - response[account_id].data[response[account_id].data.length - 4]
          : 0;
      });
    }
    return res.status(200).json(response);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the info: " + err });
  }
});

module.exports = router;
