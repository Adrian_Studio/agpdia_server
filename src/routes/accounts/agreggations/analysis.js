const express = require("express");
const router = express.Router();
const moment = require("moment");

const Account = require("../../../models/accounts/Account");
const Record = require("../../../models/accounts/Record");
const { decodeToken } = require("../../../services/manage_token");
const diacriticSensitiveRegex = require("../../../helpers/diacriticSensitiveRegex");
const currencyFormater = require("../../../helpers/currencyFormater");

const mongoose = require("mongoose");
const e = require("express");
const { response } = require("express");
const ObjectId = mongoose.Types.ObjectId;
const COLLECTION = "Account(s)";

// Get the list of documents of collection
router.post("/monthlyTable", async (req, res) => {
  const user_id = req.userId;
  const { year, concepts, accounts, type, note, group, subgroup } = req.body;
  if (!year) {
    year = moment().year();
  }
  var startYear = moment(year + "-01-01T10:00:00").startOf("year");
  var endYear = moment(year + "-01-01T10:00:00").endOf("year");
  filter = {
    user_id: user_id,
    category_id: { $nin: ["source", "destination"] },
    date: { $gte: new Date(startYear), $lte: new Date(endYear) },
  };
  if (concepts) {
    filter.concept_id = {};
    filter.concept_id.$in = concepts;
  }
  if (accounts) {
    filter.account_id = {};
    filter.account_id.$in = accounts;
  }
  if (type) {
    filter.type = {};
    filter.type.$in = type;
  }
  if (group) {
    filter.concept_group = {};
    filter.concept_group.$in = group;
  }
  if (subgroup) {
    filter.concept_subgroup = {};
    filter.concept_subgroup.$in = subgroup;
  }

  if (note) {
    if (!filter.$or) filter.$or = [];
    filter.$or.push({
      note: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      concept_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      category_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      account_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
  }

  try {
    var records = await Record.aggregate([
      {
        $match: filter,
      },
      {
        $project: {
          date: {
            $dateToString: {
              format: "%d-%m-%Y",
              date: "$date",
              timezone: "+02",
            },
          },
          day: { $dayOfMonth: { date: "$date", timezone: "+02" } },
          month: { $month: { date: "$date", timezone: "+02" } },
          year: { $year: { date: "$date", timezone: "+02" } },
          category_id: "$category_id",
          category_name: "$category_name",
          concept_id: "$concept_id",
          concept_name: "$concept_name",
          amount: "$amount",
          currency_format: "$currency_format",
          currency_symbol: "$currency_symbol",
        },
      },
      {
        $group: {
          _id: { concept_id: "$concept_id", month: "$month" },
          month: { $first: "$month" },
          concept_id: { $first: "$concept_id" },
          concept_name: { $first: "$concept_name" },
          category_id: { $first: "$category_id" },
          category_name: { $first: "$category_name" },
          amount: { $sum: "$amount" },
          currency_format: { $first: "$currency_format" },
          currency_symbol: { $first: "$currency_symbol" },
        },
      },
      { $sort: { category_name: 1, name: 1 } },
    ]);
    // console.log(records)
    var response = [];
    var array_month_labels = [
      "jan",
      "feb",
      "mar",
      "apr",
      "may",
      "jun",
      "jul",
      "ago",
      "sep",
      "oct",
      "nov",
      "dic",
    ];
    var rows = {};
    records.forEach((entry) => {
      if (!response.length) {
        response[0] = {
          label: "Balance",
          currency_format: entry.currency_format,
          currency_symbol: entry.currency_symbol,
          className: "table_balance",
          subRows: [],
        };
        // array_month_labels.forEach(element => {
        //   response[0][element] = 0
        // });
      }
      // if(!response[0].subRows[0]){
      var exist_category = response[0].subRows.some(
        (category) => category["id"] === entry.category_id
      );
      if (!exist_category) {
        var data = {
          id: entry.category_id,
          label: entry.category_name,
          currency_format: entry.currency_format,
          currency_symbol: entry.currency_symbol,
          className: "table_category",
          subRows: [],
        };
        // array_month_labels.forEach(element => {
        //   data[element] = 0
        // });
        response[0].subRows.push(data);
      }

      var category = response[0].subRows.find(
        (category) => category["id"] === entry.category_id
      );
      var exist_concept = category.subRows.some(
        (concept) => concept["id"] === entry.concept_id
      );
      if (!exist_concept) {
        var data = {
          id: entry.concept_id,
          label: entry.concept_name,
          currency_format: entry.currency_format,
          currency_symbol: entry.currency_symbol,
          className: "table_concept",
        };
        response[0].subRows.map((obj) => {
          if (obj.id == entry.category_id) {
            // array_month_labels.forEach(element => {
            //   data[element] = 0
            // });
            obj.subRows.push(data);
          }
        });
      }
      response[0].subRows.map((obj) => {
        if (obj.id == entry.category_id) {
          if (!obj[array_month_labels[entry.month - 1]]) {
            obj[array_month_labels[entry.month - 1]] = 0;
          }
          obj[array_month_labels[entry.month - 1]] += entry.amount;
          obj.subRows.map((subObj) => {
            if (subObj.id == entry.concept_id) {
              if (!subObj[array_month_labels[entry.month - 1]]) {
                subObj[array_month_labels[entry.month - 1]] = 0;
              }
              subObj[array_month_labels[entry.month - 1]] += entry.amount;
            }
          });
        }
      });
      if (!response[0][array_month_labels[entry.month - 1]]) {
        response[0][array_month_labels[entry.month - 1]] = 0;
      }
      response[0][array_month_labels[entry.month - 1]] += entry.amount;
    });
    // Currency format to all the values
    response.map((balance) => {
      balance["total"] = 0;
      array_month_labels.forEach((month) => {
        balance["total"] += balance[month] ? balance[month] : 0;
        balance[month] = currencyFormater(
          balance[month],
          balance["currency_format"],
          balance["currency_symbol"]
        );
      });
      balance["total"] = currencyFormater(
        balance["total"],
        balance["currency_format"],
        balance["currency_symbol"]
      );
      balance.subRows.map((category) => {
        category["total"] = 0;
        array_month_labels.forEach((month) => {
          category["total"] += category[month] ? category[month] : 0;
          category[month] = currencyFormater(
            category[month],
            category["currency_format"],
            category["currency_symbol"]
          );
        });
        category["total"] = currencyFormater(
          category["total"],
          category["currency_format"],
          category["currency_symbol"]
        );
        category.subRows.map((concept) => {
          concept["total"] = 0;
          array_month_labels.forEach((month) => {
            concept["total"] += concept[month] ? concept[month] : 0;
            concept[month] = currencyFormater(
              concept[month],
              concept["currency_format"],
              concept["currency_symbol"]
            );
          });
          concept["total"] = currencyFormater(
            concept["total"],
            concept["currency_format"],
            concept["currency_symbol"]
          );
        });
      });
    });

    if (!response.length)
      return res
        .status(404)
        .json({ message: "There is no data with this filter conditions" });

    return res.status(200).json(response);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the list: " + err });
  }
});

router.post("/yearsRange", async (req, res) => {
  const user_id = req.userId;
  filter = {
    user_id: user_id,
  };

  try {
    var records = await Record.aggregate([
      {
        $match: filter,
      },
      {
        $project: {
          year: { $year: { date: "$date", timezone: "+02" } },
        },
      },
      {
        $group: {
          _id: { year: "$year" },
          year: { $first: "$year" },
        },
      },
      { $sort: { year: -1 } },
    ]);
    var response = [];
    records.forEach((year) =>
      response.push({ label: year.year, value: year.year })
    );

    if (!response.length)
      return res
        .status(404)
        .json({ message: "There is no data with this filter conditions" });

    return res.status(200).json(response);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the list: " + err });
  }
});

router.post("/yearlyTable", async (req, res) => {
  const user_id = req.userId;
  const { year, concepts, accounts, type, note, group, subgroup } = req.body;
  if (!year || !year[0]) {
    var year_1 = moment().year();
  } else {
    var year_1 = year[0];
  }
  if (!year || !year[1]) {
    var year_2 = moment().year();
  } else {
    var year_2 = year[1];
  }

  var startYear = moment(year_1 + "-01-01T10:00:00").startOf("year");
  var endYear = moment(year_2 + "-01-01T10:00:00").endOf("year");
  filter = {
    user_id: user_id,
    category_id: { $nin: ["source", "destination"] },
    date: { $gte: new Date(startYear), $lte: new Date(endYear) },
  };
  if (concepts) {
    filter.concept_id = {};
    filter.concept_id.$in = concepts;
  }
  if (accounts) {
    filter.account_id = {};
    filter.account_id.$in = accounts;
  }
  if (type) {
    filter.type = {};
    filter.type.$in = type;
  }
  if (group) {
    filter.concept_group = {};
    filter.concept_group.$in = group;
  }
  if (subgroup) {
    filter.concept_subgroup = {};
    filter.concept_subgroup.$in = subgroup;
  }

  if (note) {
    if (!filter.$or) filter.$or = [];
    filter.$or.push({
      note: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      concept_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      category_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      account_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
  }

  try {
    var records = await Record.aggregate([
      {
        $match: filter,
      },
      {
        $project: {
          date: {
            $dateToString: {
              format: "%d-%m-%Y",
              date: "$date",
              timezone: "+02",
            },
          },
          day: { $dayOfMonth: { date: "$date", timezone: "+02" } },
          month: { $month: { date: "$date", timezone: "+02" } },
          year: { $year: { date: "$date", timezone: "+02" } },
          category_id: "$category_id",
          category_name: "$category_name",
          concept_id: "$concept_id",
          concept_name: "$concept_name",
          amount: "$amount",
          currency_format: "$currency_format",
          currency_symbol: "$currency_symbol",
        },
      },
      {
        $group: {
          _id: { concept_id: "$concept_id", year: "$year" },
          year: { $first: "$year" },
          concept_id: { $first: "$concept_id" },
          concept_name: { $first: "$concept_name" },
          category_id: { $first: "$category_id" },
          category_name: { $first: "$category_name" },
          amount: { $sum: "$amount" },
          currency_format: { $first: "$currency_format" },
          currency_symbol: { $first: "$currency_symbol" },
        },
      },
      { $sort: { category_name: 1, name: 1 } },
    ]);
    // console.log(records)
    var response = [];
    var array_month_labels = [
      "jan",
      "feb",
      "mar",
      "apr",
      "may",
      "jun",
      "jul",
      "ago",
      "sep",
      "oct",
      "nov",
      "dic",
    ];
    var rows = {};
    var years = [];
    records.forEach((entry) => {
      if (!years.includes(entry.year)) {
        years.push(entry.year);
      }
      if (!response.length) {
        response[0] = {
          label: "Balance",
          currency_format: entry.currency_format,
          currency_symbol: entry.currency_symbol,
          className: "table_balance",
          subRows: [],
        };
        // array_month_labels.forEach(element => {
        //   response[0][element] = 0
        // });
      }
      // if(!response[0].subRows[0]){
      var exist_category = response[0].subRows.some(
        (category) => category["id"] === entry.category_id
      );
      if (!exist_category) {
        var data = {
          id: entry.category_id,
          label: entry.category_name,
          currency_format: entry.currency_format,
          currency_symbol: entry.currency_symbol,
          className: "table_category",
          subRows: [],
        };
        // array_month_labels.forEach(element => {
        //   data[element] = 0
        // });
        response[0].subRows.push(data);
      }

      var category = response[0].subRows.find(
        (category) => category["id"] === entry.category_id
      );
      var exist_concept = category.subRows.some(
        (concept) => concept["id"] === entry.concept_id
      );
      if (!exist_concept) {
        var data = {
          id: entry.concept_id,
          label: entry.concept_name,
          currency_format: entry.currency_format,
          currency_symbol: entry.currency_symbol,
          className: "table_concept",
        };
        response[0].subRows.map((obj) => {
          if (obj.id == entry.category_id) {
            // array_month_labels.forEach(element => {
            //   data[element] = 0
            // });
            obj.subRows.push(data);
          }
        });
      }
      response[0].subRows.map((obj) => {
        if (obj.id == entry.category_id) {
          if (!obj[entry.year]) {
            obj[entry.year] = 0;
          }
          obj[entry.year] += entry.amount;
          obj.subRows.map((subObj) => {
            if (subObj.id == entry.concept_id) {
              if (!subObj[entry.year]) {
                subObj[entry.year] = 0;
              }
              subObj[entry.year] += entry.amount;
            }
          });
        }
      });
      if (!response[0][entry.year]) {
        response[0][entry.year] = 0;
      }
      response[0][entry.year] += entry.amount;
    });
    // console.log(years.sort())
    // Currency format to all the values
    response.map((balance) => {
      balance["total"] = 0;
      years.sort().forEach((year) => {
        balance["total"] += balance[year] ? balance[year] : 0;
        balance[year] = currencyFormater(
          balance[year],
          balance["currency_format"],
          balance["currency_symbol"]
        );
      });
      balance["total"] = currencyFormater(
        balance["total"],
        balance["currency_format"],
        balance["currency_symbol"]
      );
      balance.subRows.map((category) => {
        category["total"] = 0;
        years.sort().forEach((year) => {
          category["total"] += category[year] ? category[year] : 0;
          category[year] = currencyFormater(
            category[year],
            category["currency_format"],
            category["currency_symbol"]
          );
        });
        category["total"] = currencyFormater(
          category["total"],
          category["currency_format"],
          category["currency_symbol"]
        );
        category.subRows.map((concept) => {
          concept["total"] = 0;
          years.sort().forEach((year) => {
            concept["total"] += concept[year] ? concept[year] : 0;
            concept[year] = currencyFormater(
              concept[year],
              concept["currency_format"],
              concept["currency_symbol"]
            );
          });
          concept["total"] = currencyFormater(
            concept["total"],
            concept["currency_format"],
            concept["currency_symbol"]
          );
        });
      });
    });
    if (!response.length)
      return res
        .status(404)
        .json({ message: "There is no data with this filter conditions" });

    return res.status(200).json({ response, years });
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the list: " + err });
  }
});

router.post("/genericGraphs", async (req, res) => {
  const user_id = req.userId;

  const {
    start_date,
    end_date,
    grouped_by,
    date_group,
    concepts,
    accounts,
    type,
    group,
    subgroup,
    stock_graph,
    vehicles,
    houses,
  } = req.body;

  var filterRecordsHidden = {
    user_id: user_id,
  };
  var filterRecordsShown = {
    user_id: user_id,
  };
  var filterAccounts = {
    user_id: user_id,
  };

  if (concepts) {
    filterRecordsShown.concept_id = {};
    filterRecordsShown.concept_id.$in = concepts;
  }
  if (accounts) {
    filterRecordsShown.account_id = {};
    filterRecordsShown.account_id.$in = accounts;
    filterAccounts._id = {};
    filterAccounts._id.$in = [];
    accounts.forEach((id) => {
      filterAccounts._id.$in.push(ObjectId(id));
    });
  }
  if (type) {
    filterRecordsShown.type = {};
    filterRecordsShown.type.$in = type;
  }
  if (group) {
    filterRecordsShown.concept_group = {};
    filterRecordsShown.concept_group.$in = group;
  }
  if (subgroup) {
    filterRecordsShown.concept_subgroup = {};
    filterRecordsShown.concept_subgroup.$in = subgroup;
  }
  if (vehicles) {
    filterRecordsShown.vehicle_id = {};
    filterRecordsShown.vehicle_id.$in = vehicles;
  }
  if (houses) {
    filterRecordsShown.house_id = {};
    filterRecordsShown.house_id.$in = houses;
  }

  var groupBy = { year: "$year" };
  if (date_group == "year") {
  }
  if (date_group == "month") {
    groupBy.month = "$month";
  }
  if (date_group == "day") {
    groupBy.month = "$month";
    groupBy.day = "$day";
  }
  filterRecordsHidden.date = {
    $lt: new Date(start_date),
  };
  filterRecordsShown.date = {
    $gte: new Date(start_date),
    $lte: new Date(end_date),
  };

  groupBy.group = "$group"; //The group defined in grouped_by
  try {
    var accountsInitialAmount = await Account.aggregate([
      {
        $match: filterAccounts,
      },
      { $sort: { name: 1 } },
    ]);

    var accountsBalanceHidden = await Record.aggregate([
      {
        $match: filterRecordsHidden,
      },
      {
        $group: {
          _id: "$account_id",
          balance: { $sum: "$amount" },
        },
      },
    ]);

    var groups = await Record.aggregate([
      {
        $match: filterRecordsShown,
      },
      {
        $group: {
          _id: "$" + grouped_by,
          group_name: { $first: "$" + grouped_by.replace("_id", "_name") },
        },
      },
      { $sort: { group_name: 1 } },
    ]);

    var balanceShown = await Record.aggregate([
      {
        $match: filterRecordsShown,
      },
      {
        $project: {
          date: {
            $dateToString: {
              format: "%d-%m-%Y",
              date: "$date",
              timezone: "+02",
            },
          },
          day: { $dayOfMonth: { date: "$date", timezone: "+02" } },
          month: { $month: { date: "$date", timezone: "+02" } },
          year: { $year: { date: "$date", timezone: "+02" } },
          account: "$account_id",
          account_name: "$account_name",
          amount: "$amount",
          note: "$note",
          group: "$" + grouped_by,
          group_name: "$" + grouped_by.replace("_id", "_name"),
        },
      },
      {
        $group: {
          _id: groupBy,
          amount: { $sum: "$amount" },
          group_name: { $first: "$group_name" },
        },
      },
      { $sort: { _id: 1 } },
    ]);
    var response = {
      label: [],
      tooltip: [],
      balance: [],
      datasets: {},
      format: accountsInitialAmount[0] && accountsInitialAmount[0].format,
      symbol: accountsInitialAmount[0] && accountsInitialAmount[0].symbol,
    };
    var tempBalance = {};
    var temp = {};
    var tempHidden = {};
    if (grouped_by == "account_id") {
      var excludedAccounts = [];
      temp["total"] = 0;
      temp["total_visible"] = 0;
      if (accountsInitialAmount.length > 1) {
        response.datasets["total"] = {
          balance: [],
          color: "#a1ae2590",
          // format: element.format,
          // symbol: element.symbol,
          name: "T. REAL",
        };
        if (!concepts && !accounts && !type && !group && !subgroup) {
          response.datasets["total_visible"] = {
            balance: [],
            color: "#a1ae25",
            // format: element.format,
            // symbol: element.symbol,
            name: "T. DISPONIBLE",
          };
        }
      }
      var accountsWithCashflow = balanceShown
        .map((value) => value._id.group)
        .filter((value, index, self) => {
          return self.indexOf(value) === index;
        });
      accountsInitialAmount.forEach((element) => {
        if (["stock"].includes(element.statistics)) {
          excludedAccounts.push(element._id.toString());
        }
        temp[element._id] = 0;
        tempBalance[element._id] = 0;
        tempHidden[element._id] = accountsBalanceHidden.find(
          (account) => account._id == element._id
        );
        if (
          (!stock_graph &&
            !concepts &&
            !group &&
            !subgroup &&
            !accounts &&
            !type) ||
          (stock_graph && element.statistics == "stock")
        ) {
          if (tempHidden[element._id]) {
            tempBalance[element._id] = tempHidden[element._id].balance;
          }
          temp[element._id] = element.initial_amount + tempBalance[element._id];
        }
        temp["total"] += temp[element._id];
        if (!excludedAccounts.includes(element._id.toString())) {
          temp["total_visible"] += temp[element._id];
        }

        if (
          (!stock_graph &&
            accountsWithCashflow.includes(element._id.toString())) ||
          (stock_graph && element.statistics == "stock")
        ) {
          response.datasets[element._id] = {
            balance: [],
            color: element.color,
            // format: element.format,
            // symbol: element.symbol,
            name: element.name,
          };
        }
      });
      response.label.push(moment(start_date));
      Object.keys(response.datasets).forEach((dataset_id) => {
        response.datasets[dataset_id].balance.push(null);
      });
    } else {
      groups.forEach((element) => {
        if (!["source", "destination", "transfer"].includes(element._id)) {
          response.datasets[element._id] = {
            balance: [],
            //   color: element.color,
            //   // format: element.format,
            //   // symbol: element.symbol,
            name: element.name,
          };
        }
      });
      if (!stock_graph) {
        var group_unique = balanceShown.filter(
          (value, index, _arr) =>
            _arr.indexOf(value) == index &&
            !["source", "destination", "transfer"].includes(value._id.group)
        );
        group_unique.forEach((element) => {
          temp[element._id.group] = 0;
          response.datasets[element._id.group] = {
            balance: [],
            // color: element.color,
            // format: element.format,
            // symbol: element.symbol,
            name: element.group_name,
          };
        });
        response.label.push(moment(start_date));
        Object.keys(response.datasets).forEach((dataset_id) => {
          response.datasets[dataset_id].balance.push(null);
        });
      }
    }

    if (balanceShown[0]) {
      // var month_0 = balanceShown[0]._id.month ? balanceShown[0]._id.month : 1;
      // var day_0 = balanceShown[0]._id.day ? balanceShown[0]._id.day : 1;
      // var date_0 = new Date(
      //   moment(
      //     balanceShown[0]._id.year + "/" + month_0 + "/" + day_0,
      //     "YYYY-MM-DD"
      //   )
      // );
      // // if (filterRecordsShown.date.$gt < date_0) {
      // response.label.push(date_0);
      // Object.keys(response.datasets).forEach((dataset_id) => {
      //   if (grouped_by == "account_id") {
      //     response.datasets[dataset_id].balance.push(
      //       Number(temp[dataset_id].toFixed(2))
      //     );
      //   } else {
      //     response.datasets[dataset_id].balance.push(null);
      //   }
      // });
      // }
      balanceShown.forEach((element) => {
        // console.log(element)
        // element.amount = Number(element.amount.toFixed(2))
        // console.log(element)
        temp[element._id.group] += Number(element.amount.toFixed(2));
        temp["total"] += Number(element.amount.toFixed(2));
        if (
          grouped_by == "account_id" &&
          !excludedAccounts.includes(element._id.group)
        ) {
          temp["total_visible"] += Number(element.amount.toFixed(2));
        }
        var month = element._id.month ? element._id.month : 1;
        var day = element._id.day ? element._id.day : 1;
        var date = moment(
          element._id.year + "/" + month + "/" + day,
          "YYYY-MM-DD"
        );
        if (date_group == "year") date = new Date(date.endOf("year"));
        if (date_group == "month") date = new Date(date.endOf("month"));
        if (date_group == "day") date = new Date(date.endOf("day"));
        var newDate = false;

        if (!response.label.includes(date.toString())) {
          response.label.push(date.toString());
          newDate = true;
        }
        // console.log(temp)
        Object.keys(response.datasets).forEach((dataset_id) => {
          if (newDate) {
            if (grouped_by == "account_id") {
              response.datasets[dataset_id].balance.push(
                Number(temp[dataset_id].toFixed(2))
              );
            } else {
              if (dataset_id == element._id.group) {
                response.datasets[dataset_id].balance.push(
                  Number(element.amount.toFixed(2))
                );
              } else {
                response.datasets[dataset_id].balance.push(null);
              }
            }
          } else {
            if (grouped_by == "account_id") {
              response.datasets[dataset_id].balance[
                response.datasets[dataset_id].balance.length - 1
              ] = Number(temp[dataset_id].toFixed(2));
            } else {
              if (dataset_id == element._id.group) {
                response.datasets[dataset_id].balance[
                  response.datasets[dataset_id].balance.length - 1
                ] = Number(element.amount.toFixed(2));
              }
            }
          }
        });
        // console.log(response.datasets)
      });
      var month_end = balanceShown[balanceShown.length - 1]._id.month
        ? balanceShown[balanceShown.length - 1]._id.month
        : 1;
      var day_end = balanceShown[balanceShown.length - 1]._id.day
        ? balanceShown[balanceShown.length - 1]._id.day
        : 1;
      var date_end = moment(
        balanceShown[balanceShown.length - 1]._id.year +
          "/" +
          month_end +
          "/" +
          day_end,
        "YYYY-MM-DD"
      );
      if (date_group == "year") date_end = new Date(date_end.endOf("year"));
      if (date_group == "month") date_end = new Date(date_end.endOf("month"));
      if (date_group == "day") date_end = new Date(date_end.endOf("day"));
      if (filterRecordsShown.date.$lte > date_end) {
        response.label.push(filterRecordsShown.date.$lte);
        Object.keys(response.datasets).forEach((dataset_id) => {
          // if(grouped_by == "account_id"){
          //   response.datasets[dataset_id].balance.push(Number(temp[dataset_id].toFixed(2)));
          // }else{
          response.datasets[dataset_id].balance.push(null);
          // }
        });
      }
    } else {
      if (date_group != "year") {
        response.label.push(filterRecordsShown.date.$gt);
        Object.keys(response.datasets).forEach((dataset_id) => {
          if (grouped_by == "account_id") {
            response.datasets[dataset_id].balance.push(
              Number(temp[dataset_id].toFixed(2))
            );
          } else {
            response.datasets[dataset_id].balance.push(null);
          }
        });
      } else {
        response.label.push(
          new Date(
            moment(filterRecordsShown.date.$lte)
              .startOf("year")
              .startOf("month")
              .startOf("day")
          )
        );
        Object.keys(response.datasets).forEach((dataset_id) => {
          if (grouped_by == "account_id") {
            response.datasets[dataset_id].balance.push(
              Number(temp[dataset_id].toFixed(2))
            );
          } else {
            response.datasets[dataset_id].balance.push(null);
          }
        });
      }

      response.label.push(moment(end_date));
      Object.keys(response.datasets).forEach((dataset_id) => {
        response.datasets[dataset_id].balance.push(null);
      });
      // response.label.push(filterRecordsShown.date.$lte);
      // Object.keys(response.datasets).forEach((dataset_id)=>{
      //   if(grouped_by == "account_id"){
      //     response.datasets[dataset_id].balance.push(Number(temp[dataset_id].toFixed(2)));
      //   }else{
      //     response.datasets[dataset_id].balance.push(null);
      //   }
      // })
    }
    // if (!balanceShown.length) return res.status(200).json(0);
    // console.log(response)
    return res.status(200).json(response);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the info: " + err });
  }
});

router.post("/invoiceGraph", async (req, res) => {
  const user_id = req.userId;

  const {
    start_date,
    end_date,
    grouped_by,
    date_group,
    concepts,
    accounts,
    type,
    group,
    subgroup,
    stock_graph,
    vehicles,
    houses,
  } = req.body;

  var filterRecordsHidden = {
    user_id: user_id,
  };
  var filterRecordsShown = {
    user_id: user_id,
  };
  var filterAccounts = {
    user_id: user_id,
  };

  if (concepts) {
    filterRecordsShown.concept_id = {};
    filterRecordsShown.concept_id.$in = concepts;
  }
  if (accounts) {
    filterRecordsShown.account_id = {};
    filterRecordsShown.account_id.$in = accounts;
    filterAccounts._id = {};
    filterAccounts._id.$in = [];
    accounts.forEach((id) => {
      filterAccounts._id.$in.push(ObjectId(id));
    });
  }
  if (type) {
    filterRecordsShown.type = {};
    filterRecordsShown.type.$in = type;
  }
  if (group) {
    filterRecordsShown.concept_group = {};
    filterRecordsShown.concept_group.$in = group;
  }
  if (subgroup) {
    filterRecordsShown.concept_subgroup = {};
    filterRecordsShown.concept_subgroup.$in = subgroup;
  }
  if (vehicles) {
    filterRecordsShown.vehicle_id = {};
    filterRecordsShown.vehicle_id.$in = vehicles;
  }
  if (houses) {
    filterRecordsShown.house_id = {};
    filterRecordsShown.house_id.$in = houses;
  }

  var groupBy = { year: "$year" };
  if (date_group == "year") {
  }
  if (date_group == "month") {
    groupBy.month = "$month";
  }
  if (date_group == "day") {
    groupBy.month = "$month";
    groupBy.day = "$day";
  }
  filterRecordsHidden.date = {
    $lt: new Date(start_date),
  };
  filterRecordsShown.date = {
    $gte: new Date(start_date),
    $lte: new Date(end_date),
  };

  groupBy.group = "$group"; //The group defined in grouped_by
  try {
    var accountsInitialAmount = await Account.aggregate([
      {
        $match: filterAccounts,
      },
    ]);

    // var accountsBalanceHidden = await Record.aggregate([
    //   {
    //     $match: filterRecordsHidden,
    //   },
    //   {
    //     $group: {
    //       _id: "$account_id",
    //       balance: { $sum: "$amount" },
    //     },
    //   },
    // ]);

    var balanceShown = await Record.aggregate([
      {
        $match: filterRecordsShown,
      },
      {
        $project: {
          date: {
            $dateToString: {
              format: "%d-%m-%Y",
              date: "$date",
              timezone: "+02",
            },
          },
          day: { $dayOfMonth: { date: "$date", timezone: "+02" } },
          month: { $month: { date: "$date", timezone: "+02" } },
          year: { $year: { date: "$date", timezone: "+02" } },
          account: "$account_id",
          account_name: "$account_name",
          amount: "$amount",
          note: "$note",
          consumption: "$consumption",
          consumption_ud: "$consumption_ud",
          group: "$" + grouped_by,
          group_name: "$" + grouped_by.replace("_id", "_name"),
        },
      },
      {
        $group: {
          _id: groupBy,
          amount: { $sum: "$amount" },
          group_name: { $first: "$group_name" },
          consumption: { $sum: "$consumption" },
          consumption_ud: { $first: "$consumption_ud" },
        },
      },
      { $sort: { _id: 1 } },
    ]);

    var response = {
      label: [],
      tooltip: [],
      balance: [],
      datasets: {},
      format: accountsInitialAmount[0] && accountsInitialAmount[0].format,
      symbol: accountsInitialAmount[0] && accountsInitialAmount[0].symbol,
    };
    var amount;
    response.datasets["amount"] = {
      balance: [],
      color: "#339933",
      // format: element.format,
      // symbol: element.symbol,
      name: "Importe",
      type: "line",
      yAxisID: "amount",
    };
    var group_unique = balanceShown.filter(
      (value, index, _arr) =>
        _arr.indexOf(value) == index &&
        !["source", "destination", "transfer"].includes(value._id.group)
    );
    group_unique.forEach((element) => {
      response.datasets[element._id.group] = {
        balance: [],
        // color: element.color,
        // format: element.format,
        // symbol: element.symbol,
        name: element.group_name,
        yAxisID: "consumption",
      };
    });
    response.label.push(moment(start_date));
    Object.keys(response.datasets).forEach((dataset_id) => {
      response.datasets[dataset_id].balance.push(null);
    });

    if (balanceShown[0]) {
      response.units = balanceShown[0].consumption_ud;
      // var month_0 = balanceShown[0]._id.month ? balanceShown[0]._id.month : 1;
      // var day_0 = balanceShown[0]._id.day ? balanceShown[0]._id.day : 1;
      // var date_0 = new Date(
      //   moment(
      //     balanceShown[0]._id.year + "/" + month_0 + "/" + day_0,
      //     "YYYY-MM-DD"
      //   )
      // );
      // response.label.push(date_0);
      // Object.keys(response.datasets).forEach((dataset_id) => {
      //   response.datasets[dataset_id].balance.push(null);
      // });
      balanceShown.forEach((element) => {
        var month = element._id.month ? element._id.month : 1;
        var day = element._id.day ? element._id.day : 1;
        var date = moment(
          element._id.year + "/" + month + "/" + day,
          "YYYY-MM-DD"
        );
        if (date_group == "year") date = new Date(date.endOf("year"));
        if (date_group == "month") date = new Date(date.endOf("month"));
        if (date_group == "day") date = new Date(date.endOf("day"));
        var newDate = false;

        if (!response.label.includes(date.toString())) {
          response.label.push(date.toString());
          newDate = true;
          amount = -Number(element.amount.toFixed(2));
        }

        Object.keys(response.datasets).forEach((dataset_id) => {
          if (newDate) {
            if (dataset_id == element._id.group) {
              response.datasets[dataset_id].balance.push(
                Number(element.consumption.toFixed(2))
              );
            } else {
              response.datasets[dataset_id].balance.push(null);
            }
          } else {
            if (dataset_id == element._id.group) {
              response.datasets[dataset_id].balance[
                response.datasets[dataset_id].balance.length - 1
              ] = Number(element.consumption.toFixed(2));
              amount -= Number(element.consumption.toFixed(2));
            }
          }
        });
        response.datasets["amount"].balance[
          response.datasets["amount"].balance.length - 1
        ] = Number(amount);
      });
      // var month_end = balanceShown[balanceShown.length - 1]._id.month
      //   ? balanceShown[balanceShown.length - 1]._id.month
      //   : 1;
      // var day_end = balanceShown[balanceShown.length - 1]._id.day
      //   ? balanceShown[balanceShown.length - 1]._id.day
      //   : 1;
      // var date_end = moment(
      //   balanceShown[balanceShown.length - 1]._id.year +
      //     "/" +
      //     month_end +
      //     "/" +
      //     day_end,
      //   "YYYY-MM-DD"
      // );
      // if (date_group == "year") date_end = new Date(date_end.endOf("year"));
      // if (date_group == "month") date_end = new Date(date_end.endOf("month"));
      // if (date_group == "day") date_end = new Date(date_end.endOf("day"));
      // if (filterRecordsShown.date.$lte > date_end) {
      //   response.label.push(filterRecordsShown.date.$lte);
      //   Object.keys(response.datasets).forEach((dataset_id) => {
      //     response.datasets[dataset_id].balance.push(null);
      //   });
      // }
    } else {
      if (date_group != "year") {
        response.label.push(filterRecordsShown.date.$gt);
        Object.keys(response.datasets).forEach((dataset_id) => {
          response.datasets[dataset_id].balance.push(null);
        });
      } else {
        response.label.push(
          new Date(
            moment(filterRecordsShown.date.$lte)
              .startOf("year")
              .startOf("month")
              .startOf("day")
          )
        );
        Object.keys(response.datasets).forEach((dataset_id) => {
          response.datasets[dataset_id].balance.push(null);
        });
      }
    }

    response.label.push(moment(end_date));
    Object.keys(response.datasets).forEach((dataset_id) => {
      response.datasets[dataset_id].balance.push(null);
    });

    return res.status(200).json(response);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the info: " + err });
  }
});

router.post("/vehicleCards", async (req, res) => {
  const user_id = req.userId;

  const {
    start_date,
    end_date,
    grouped_by,
    date_group,
    concepts,
    accounts,
    type,
    group,
    subgroup,
    stock_graph,
    vehicles,
    houses,
  } = req.body;

  var filterRecordsHidden = {
    user_id: user_id,
  };
  var filterRecordsShown = {
    user_id: user_id,
  };
  var filterAccounts = {
    user_id: user_id,
  };

  // if (concepts) {
  //   filterRecordsShown.concept_id = {};
  //   filterRecordsShown.concept_id.$in = concepts;
  // }
  // if (accounts) {
  //   filterRecordsShown.account_id = {};
  //   filterRecordsShown.account_id.$in = accounts;
  //   filterAccounts._id = {};
  //   filterAccounts._id.$in = [];
  //   accounts.forEach((id) => {
  //     filterAccounts._id.$in.push(ObjectId(id));
  //   });
  // }
  // if (type) {
  //   filterRecordsShown.type = {};
  //   filterRecordsShown.type.$in = type;
  // }
  if (group) {
    filterRecordsShown.concept_group = {};
    filterRecordsShown.concept_group.$in = group;
  }
  // if (subgroup) {
  //   filterRecordsShown.concept_subgroup = {};
  //   filterRecordsShown.concept_subgroup.$in = subgroup;
  // }
  if (vehicles) {
    filterRecordsShown.vehicle_id = {};
    filterRecordsShown.vehicle_id.$in = vehicles;
  }
  // if (houses) {
  //   filterRecordsShown.house_id = {};
  //   filterRecordsShown.house_id.$in = houses;
  // }

  // var groupBy = { year: "$year" };
  // if (date_group == "year") {
  // }
  // if (date_group == "month") {
  //   groupBy.month = "$month";
  // }
  // if (date_group == "day") {
  //   groupBy.month = "$month";
  //   groupBy.day = "$day";
  // }
  // filterRecordsHidden.date = {
  //   $lt: new Date(start_date),
  // };
  // filterRecordsShown.date = {
  //   $gte: new Date(start_date),
  //   $lte: new Date(end_date),
  // };

  // groupBy.group = "$group"; //The group defined in grouped_by
  try {
    var accountsInitialAmount = await Account.aggregate([
      {
        $match: filterAccounts,
      },
    ]);

    var balanceShown = await Record.aggregate([
      {
        $match: filterRecordsShown,
      },
    ]);

    var grossData = {};

    if (balanceShown[0]) {
      balanceShown.forEach((element) => {
        if (!grossData[element.vehicle_id]) {
          grossData[element.vehicle_id] = {
            amount_total: 0,
            amount_maintenance: 0,
            amount_power_source: 0,
            amount_payment: 0,
            total_km: 0,
            total_l: 0,
          };
        }
        grossData[element.vehicle_id].currency_format = element.currency_format;
        grossData[element.vehicle_id].currency_symbol = element.currency_symbol;
        if (element.concept_subgroup == "power_source_vehicle") {
          // console.log(element.consumption)
          grossData[element.vehicle_id].total_l += element.consumption;
          grossData[element.vehicle_id].consumption_ud = element.consumption_ud;
          grossData[element.vehicle_id].amount_power_source -= element.amount;
          grossData[element.vehicle_id].amount_total -= element.amount;
          if (grossData[element.vehicle_id].total_km < element.milometer) {
            grossData[element.vehicle_id].total_km = element.milometer;

            if (grossData[element.vehicle_id].total_km_prev != 0) {
              grossData[element.vehicle_id].total_km_prev = element.milometer;
            }
          } else if (
            grossData[element.vehicle_id].total_km_prev > element.milometer ||
            element.milometer == 0
          ) {
            grossData[element.vehicle_id].total_km_prev = element.milometer;
            grossData[element.vehicle_id].first_fuel = element.consumption;
          }
        } else if (element.concept_subgroup == "maintenance_vehicle") {
          grossData[element.vehicle_id].amount_maintenance -= element.amount;
          grossData[element.vehicle_id].amount_total -= element.amount;
        } else {
          grossData[element.vehicle_id].amount_payment -= element.amount;
          grossData[element.vehicle_id].amount_total -= element.amount;
        }
      });
    }
    var response = {
      total_kilometers: 0,
      total_amount: 0,
      total_amount_mtto: 0,
      total_amount_pay: 0,
      consumption: 0,
      kilometer_price: 0,
    };
    var aplicable_consumption = 0;
    Object.keys(grossData).forEach((element) => {
      response.total_amount += grossData[element].amount_total;
      response.total_amount_mtto +=
        grossData[element].amount_maintenance +
        grossData[element].amount_power_source;
      response.total_amount_pay += grossData[element].amount_payment;
      response.total_kilometers += grossData[element].total_km;
      aplicable_consumption +=
        grossData[element].total_l - grossData[element].first_fuel;
      response.currency_format = grossData[element].currency_format;
      response.currency_symbol = grossData[element].currency_symbol;
      response.currency_symbol = grossData[element].currency_symbol;
      if (
        response.consumption_ud &&
        response.consumption_ud != grossData[element].consumption_ud
      ) {
        response.consumption_ud = grossData[element].consumption_ud;
      } else {
        response.consumption_ud = "und";
      }
    });

    response.consumption = !response.total_kilometers
      ? 0
      : (100 * aplicable_consumption) / response.total_kilometers;
    response.kilometer_price = !response.total_kilometers
      ? 0
      : response.total_amount / response.total_kilometers;
    return res.status(200).json(response);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the info: " + err });
  }
});

module.exports = router;
