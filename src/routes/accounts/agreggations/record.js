const express = require("express");
const router = express.Router();

const Record = require("../../../models/accounts/Record");
const { decodeToken } = require("../../../services/manage_token");
const diacriticSensitiveRegex = require("../../../helpers/diacriticSensitiveRegex");

const COLLECTION = "Record(s)";

// Get the list of documents of collection
router.post("/list", async (req, res) => {
  const user_id = req.userId;
  const { start_date, end_date, concepts, accounts, type, note, group, subgroup, vehicles, houses } = req.body;
  filter = {
    user_id: user_id,
  };
  if(start_date || end_date) filter.date= { $gte: new Date(start_date), $lte: new Date(end_date) }
  if (concepts) {
    filter.concept_id = {};
    filter.concept_id.$in = concepts;
  }
  if (accounts) {
    filter.account_id = {};
    filter.account_id.$in = accounts;
  }
  if (type){
    filter.type = {};
    filter.type.$in = type;
  }
  if (group){
    filter.concept_group = {};
    filter.concept_group.$in = group;
  }
  if (subgroup){
    filter.concept_subgroup = {};
    filter.concept_subgroup.$in = subgroup;
  }
  if (vehicles){
    filter.vehicle_id = {};
    filter.vehicle_id.$in = vehicles;
  }
  if (houses){
    filter.house_id = {};
    filter.house_id.$in = houses;
  }

  if (note) {
    if (!filter.$or) filter.$or = [];
    filter.$or.push({
      note: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      concept_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      category_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      account_name: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
  }

  try {
    var recordsList = await Record.aggregate([
      {
        $match: filter,
      },
      { $sort: { _id: -1 } },
      {
        $group: {
          _id: "$date",
          dailyTotal: { $sum: "$amount" },
          records: {
            $push: {
              id: "$_id",
              date: { $dateToString: { format: "%m-%d-%Y", date: "$_id" } },
              category: "$category_name",
              concept: "$concept_name",
              note: "$note",
              amount: "$amount",
              account: "$account_name",
              account_color: "$account_color",
              currency_symbol: "$currency_symbol",
              currency_format: "$currency_format",
              has_main_currency: "$has_main_currency",
              transfer_oid: "$transfer_oid",
            },
          },
        },
      },
      {
        $addFields: {
          date: "$_id",
        },
      },
      { $sort: { _id: -1 } },
      {
        $group: {
          _id: null,
          total: { $sum: "$dailyTotal" },
          daily: {
            $push: {
              id: "$_id",
              dailyTotal: "$dailyTotal",
              records: "$records",
              date: "$date",
            },
          },
        },
      },
    ]);

    if (!recordsList.length)
      return res
        .status(404)
        .json({ message: "There is no data with this filter conditions" });

    return res.status(200).json(recordsList[0]);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the list: " + err });
  }
});

// Amount by category
router.get("/concepts_pie", async (req, res) => {
  const user_id = req.userId;

  filter = {
    user_id: user_id,
  };

  var categories = await Record.aggregate([
    {
      $match: filter,
    },
    {
      $group: {
        _id: {
          category: "$category_name",
          currency: "$currency_symbol",
          has_main_currency: "$has_main_currency",
        },
        count: { $sum: "$amount" },
        // concepts: { $push: "$$ROOT" },
      },
    },
    { $sort: { _id: 1 } },
  ]);

  var concepts = await Record.aggregate([
    {
      $group: {
        _id: {
          category: "$category_name",
          concept: "$concept_name",
          currency: "$currency_symbol",
          has_main_currency: "$has_main_currency",
        },
        count: { $sum: "$amount" },
        // concepts: { $push: "$$ROOT" },
      },
    },
    { $sort: { _id: 1 } },
  ]);

  res.status(200).json({ categories: categories, concepts: concepts });
});

module.exports = router;
