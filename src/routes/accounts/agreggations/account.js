const express = require("express");
const router = express.Router();
const moment = require("moment");

const Account = require("../../../models/accounts/Account");
const Record = require("../../../models/accounts/Record");
const { decodeToken } = require("../../../services/manage_token");
const diacriticSensitiveRegex = require("../../../helpers/diacriticSensitiveRegex");

const mongoose = require("mongoose");
const e = require("express");
const ObjectId = mongoose.Types.ObjectId;
const COLLECTION = "Account(s)";

// Get the list of documents of collection
router.post("/list", async (req, res) => {
  const user_id = req.userId;

  filter = {
    user_id: user_id,
  };

  try {
    var accounts = await Account.aggregate([
      {
        $match: filter,
      },
      { $sort: { name: 1 } },
    ]);
    var accountsBalance = await Record.aggregate([
      {
        $match: filter,
      },
      {
        $group: {
          _id: "$account_id",
          balance: { $sum: "$amount" },
        },
      },
    ]);
    var accountsList = [];
    accounts.forEach((account) => {
      account.amount = account.initial_amount;
      var tempBalance = accountsBalance.find(
        (accountBalance) =>
          JSON.stringify(accountBalance._id) == JSON.stringify(account._id)
      );

      if (tempBalance) account.amount += tempBalance.balance;

      accountsList.push(account);
    });
    if (!accountsList.length)
      return res
        .status(404)
        .json({ message: "There is no data with this filter conditions" });

    return res.status(200).json(accountsList);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the list: " + err });
  }
});

//Info
router.get("/balance/:id", async function (req, res) {
  const { id } = req.params;
  const user_id = req.userId;
  filter = {
    user_id: user_id,
    account_id: id,
  };
  try {
    var accountBalance = await Record.aggregate([
      {
        $match: filter,
      },
      {
        $group: {
          _id: null,
          total: { $sum: "$amount" },
        },
      },
    ]);

    if (!accountBalance.length) return res.status(200).json(0);

    return res.status(200).json(accountBalance[0].total);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the info: " + err });
  }
});

//Line Graph
router.post("/line_graph_account", async function (req, res) {
  const user_id = req.userId;
  const { group, account_id } = req.body;
  var filterRecordsHidden = {
    user_id: user_id,
  };
  var filterRecordsShown = {
    user_id: user_id,
  };
  var filterAccounts = {
    user_id: user_id,
  };

  if (account_id) {
    filterAccounts._id = ObjectId(account_id);
    filterRecordsHidden.account_id = account_id;
    filterRecordsShown.account_id = account_id;
  } else {
    return res.status(400).json({ message: "Need to specify account" });
  }

  var groupBy = { year: "$year" };
  if (group == "year") {
    filterRecordsHidden = { _id: false };
    filterRecordsShown.date = {
      $lte: new Date(moment().endOf("year").endOf("month").endOf("day")),
    };
  }
  if (group == "month") {
    groupBy.month = "$month";

    filterRecordsHidden.date = {
      $lte: new Date(
        moment()
          .subtract(1, "years")
          .endOf("month")
          .add(1, "days")
          .startOf("day")
      ),
    };
    filterRecordsShown.date = {
      $gt: new Date(
        moment()
          .subtract(1, "years")
          .endOf("month")
          .add(1, "days")
          .startOf("day")
      ),
      $lte: new Date(moment().endOf("month").endOf("day")),
    };
  }
  if (group == "day") {
    groupBy.month = "$month";
    groupBy.day = "$day";
    filterRecordsHidden.date = {
      $lt: new Date(moment().subtract(1, "months").startOf("day")),
    };
    filterRecordsShown.date = {
      $gt: new Date(moment().subtract(1, "months").startOf("day")),
      $lte: new Date(moment().endOf("day")),
    };
  }
  try {
    var accountsInitialAmount = await Account.aggregate([
      {
        $match: filterAccounts,
      },
    ]);
    var accountsBalanceHidden = await Record.aggregate([
      {
        $match: filterRecordsHidden,
      },
      {
        $group: {
          _id: null,
          balance: { $sum: "$amount" },
        },
      },
    ]);
    var accountsBalanceShown = await Record.aggregate([
      {
        $match: filterRecordsShown,
      },
      {
        $project: {
          date: {
            $dateToString: {
              format: "%d-%m-%Y",
              date: "$date",
              timezone: "+02",
            },
          },
          day: { $dayOfMonth: { date: "$date", timezone: "+02" } },
          month: { $month: { date: "$date", timezone: "+02" } },
          year: { $year: { date: "$date", timezone: "+02" } },
          account: "$account_id",
          account_name: "$account_name",
          amount: "$amount",
          note: "$note",
        },
      },
      {
        $group: {
          _id: groupBy,
          amount: { $sum: "$amount" },
        },
      },
      { $sort: { _id: 1 } },
    ]);
    var tempBalance = 0;
    if (accountsBalanceHidden[0])
      tempBalance = accountsBalanceHidden[0].balance;
    var temp = accountsInitialAmount[0].initial_amount + tempBalance;
    var response = {
      label: [],
      balance: [],
      color: accountsInitialAmount[0].color,
      format: accountsInitialAmount[0].format,
      symbol: accountsInitialAmount[0].symbol,
    };
    if (accountsBalanceShown[0]) {
      var month_0 = accountsBalanceShown[0]._id.month
        ? accountsBalanceShown[0]._id.month
        : 1;
      var day_0 = accountsBalanceShown[0]._id.day
        ? accountsBalanceShown[0]._id.day
        : 1;
      var date_0 = new Date(
        moment(
          accountsBalanceShown[0]._id.year + "/" + month_0 + "/" + day_0,
          "YYYY-MM-DD"
        )
      );
      if (filterRecordsShown.date.$gt < date_0) {
        response.label.push(filterRecordsShown.date.$gt);
        response.balance.push(Number(temp.toFixed(2)));
      } else if (group == "year") {
        var month = accountsBalanceShown[0]._id.month
          ? accountsBalanceShown[0]._id.month
          : 1;
        var day = accountsBalanceShown[0]._id.day
          ? accountsBalanceShown[0]._id.day
          : 1;
        var date = new Date(
          moment(
            accountsBalanceShown[0]._id.year + "/" + month + "/" + day,
            "YYYY-MM-DD"
          )
        );
        response.label.push(date);
        response.balance.push(Number(temp.toFixed(2)));
      }

      accountsBalanceShown.forEach((element) => {
        // element.amount = Number(element.amount.toFixed(2))
        temp += Number(element.amount.toFixed(2));

        var month = element._id.month ? element._id.month : 1;
        var day = element._id.day ? element._id.day : 1;
        var date = moment(
          element._id.year + "/" + month + "/" + day,
          "YYYY-MM-DD"
        );
        if (group == "year") date = new Date(date.endOf("year"));
        if (group == "month") date = new Date(date.endOf("month"));
        if (group == "day") date = new Date(date.endOf("day"));
        response.label.push(date);
        response.balance.push(Number(temp.toFixed(2)));
      });
      var month_end = accountsBalanceShown[accountsBalanceShown.length - 1]._id
        .month
        ? accountsBalanceShown[accountsBalanceShown.length - 1]._id.month
        : 1;
      var day_end = accountsBalanceShown[accountsBalanceShown.length - 1]._id
        .day
        ? accountsBalanceShown[accountsBalanceShown.length - 1]._id.day
        : 1;
      var date_end = moment(
        accountsBalanceShown[accountsBalanceShown.length - 1]._id.year +
          "/" +
          month_end +
          "/" +
          day_end,
        "YYYY-MM-DD"
      );
      if (group == "year") date_end = new Date(date_end.endOf("year"));
      if (group == "month") date_end = new Date(date_end.endOf("month"));
      if (group == "day") date_end = new Date(date_end.endOf("day"));
      if (filterRecordsShown.date.$lte > date_end) {
        response.label.push(filterRecordsShown.date.$lte);
        response.balance.push(Number(temp.toFixed(2)));
      }
    } else {
      if (group != "year") {
        response.label.push(filterRecordsShown.date.$gt);
        response.balance.push(Number(temp.toFixed(2)));
      } else {
        response.label.push(
          new Date(
            moment(filterRecordsShown.date.$lte)
              .startOf("year")
              .startOf("month")
              .startOf("day")
          )
        );
        response.balance.push(Number(temp.toFixed(2)));
      }
      response.label.push(filterRecordsShown.date.$lte);
      response.balance.push(Number(temp.toFixed(2)));
    }
    // if (!accountsBalanceShown.length) return res.status(200).json(0);

    return res.status(200).json(response);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the info: " + err });
  }
});

//Bar Graph
router.post("/bar_graph_account", async function (req, res) {
  const user_id = req.userId;
  const { group, account_id, byConcept } = req.body;
  var filterRecordsHidden = {
    user_id: user_id,
  };
  var filterRecordsShown = {
    user_id: user_id,
  };
  var filterAccounts = {
    user_id: user_id,
  };

  if (account_id) {
    filterAccounts._id = ObjectId(account_id);
    filterRecordsHidden.account_id = account_id;
    filterRecordsShown.account_id = account_id;
  } else {
    return res.status(400).json({ message: "Need to specify account" });
  }

  var groupBy = { year: "$year" };
  if (group == "year") {
    filterRecordsHidden = { _id: false };
    filterRecordsShown.date = {
      $lte: new Date(moment().endOf("year").endOf("month").endOf("day")),
    };
  }
  if (group == "month") {
    groupBy.month = "$month";

    filterRecordsHidden.date = {
      $lte: new Date(
        moment()
          .subtract(1, "years")
          .endOf("month")
          .add(1, "days")
          .startOf("day")
      ),
    };
    filterRecordsShown.date = {
      $gt: new Date(
        moment()
          .subtract(1, "years")
          .endOf("month")
          .add(1, "days")
          .startOf("day")
      ),
      $lte: new Date(moment().endOf("month").endOf("day")),
    };
  }
  if (group == "day") {
    groupBy.month = "$month";
    groupBy.day = "$day";
    filterRecordsHidden.date = {
      $lt: new Date(moment().subtract(1, "months").startOf("day")),
    };
    filterRecordsShown.date = {
      $gt: new Date(moment().subtract(1, "months").startOf("day")),
      $lte: new Date(moment().endOf("day")),
    };
  }
  if (byConcept) {
    groupBy.key = "$concept_id";
  } else {
    groupBy.key = "$category_id";
  }

  try {
    var accounts = await Account.aggregate([
      {
        $match: filterAccounts,
      },
    ]);
    // var accountsBalanceHidden = await Record.aggregate([
    //   {
    //     $match: filterRecordsHidden,
    //   },
    //   {
    //     $group: {
    //       _id: null,
    //       balance: { $sum: "$amount" },
    //     },
    //   },
    // ]);
    var accountsBalanceShown = await Record.aggregate([
      {
        $match: filterRecordsShown,
      },
      {
        $project: {
          date: {
            $dateToString: {
              format: "%d-%m-%Y",
              date: "$date",
              timezone: "+02",
            },
          },
          day: { $dayOfMonth: { date: "$date", timezone: "+02" } },
          month: { $month: { date: "$date", timezone: "+02" } },
          year: { $year: { date: "$date", timezone: "+02" } },
          account: "$account_id",
          account_name: "$account_name",
          category_id: "$category_id",
          category_name: "$category_name",
          concept_id: "$concept_id",
          concept_name: "$concept_name",
          amount: "$amount",
          note: "$note",
        },
      },
      {
        $group: {
          _id: groupBy,
          amount: { $sum: "$amount" },
        },
      },
      { $sort: { _id: 1 } },
    ]);
    var tempBalance = 0;
    var response = {
      label: [],
      datasets: {},
      color: accounts[0].color,
      format: accounts[0].format,
      symbol: accounts[0].symbol,
    };
    if (accountsBalanceShown[0]) {
      accountsBalanceShown.forEach((element) => {
        if (!response.datasets[element._id.key])
          response.datasets[element._id.key] = [];
        var month = element._id.month ? element._id.month : 1;
        var day = element._id.day ? element._id.day : 1;
        var date = moment(
          element._id.year + "/" + month + "/" + day,
          "YYYY-MM-DD"
        );

        if (group == "year") date = new Date(date.endOf("year"));
        if (group == "month") date = new Date(date.endOf("month"));
        if (group == "day") date = new Date(date.endOf("day"));
        if (
          !response.label.length ||
          response.label[response.label.length - 1].toString() !=
            date.toString()
        ) {
          response.label.push(date);
        }
        while (
          response.label.length - 1 >
          response.datasets[element._id.key].length
        ) {
          response.datasets[element._id.key].push(null);
        }
        response.datasets[element._id.key].push(
          Number(element.amount.toFixed(2))
        );
      });
      Object.keys(response.datasets).forEach((key) => {
        while (response.label.length > response.datasets[key].length) {
          response.datasets[key].push(null);
        }
      });
    }

    return res.status(200).json(response);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the info: " + err });
  }
});

module.exports = router;
