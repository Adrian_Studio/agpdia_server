const express = require("express");
const router = express.Router();

const AccountsAlert = require("../../models/alerts/AccountsAlert");

const COLLECTION = "AccountsAlert(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  AccountsAlert.find({ user_id }, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
        });

      if (!result)
        return res
          .status(404)
          .json({ message: `The ${COLLECTION} doesn't exist` });

      return res.status(200).json(result);
    });
});

// Get a document
router.get("/:id", function (req, res) {
  const { id } = req.params;

  AccountsAlert.findById(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json(result);
  });
});

// Add a document
router.post("/", function (req, res) {
  const user_id = req.userId;
  const { concept_id, concept_name, periodicity_months, periods_to_alert, start_date, isActive} = req.body;

  const newAccountsAlert = new AccountsAlert({ user_id, concept_id, concept_name, periodicity_months, periods_to_alert, start_date, isActive });

  newAccountsAlert.save((err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
      });

    result.user_id = undefined;
    return res.status(200).json(result);
  });
});

// Update a document
router.put("/:id", function (req, res) {
  const { id } = req.params;
  const updateData = req.body;

  AccountsAlert.findByIdAndUpdate(
    id,
    updateData,
    { runValidators: true },
    (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
        });

      if (!result)
        return res
          .status(404)
          .json({ message: `The ${COLLECTION} doesn't exist` });

      Record.updateMany(
        { category_id: id },
        { category_name: updateData.name },
        (err, result) => {
          if (err)
            return res.status(400).json({
              message: `Error updating Records: ${err}`,
            });
          // return res.status(400).json({
          //     message: `The record has been updated successfully`,
          //   });
        }
      );
      return res.status(200).json(result);
    }
  );
});

// Delete a document
router.delete("/:id", function (req, res) {
  const { id } = req.params;

  Record.exists({ category_id: id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying checking if it's: ${err}`,
      });
    if (result)
      return res.status(400).json({
        message: `The category it's used`,
      });
    AccountsAlert.findByIdAndDelete(id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
        });

      if (!result)
        return res.status(400).json({
          message: `The ${COLLECTION} doesn't exist`,
        });

      return res
        .status(200)
        .json({ message: `The ${COLLECTION} has been deleted` });
    });
  });
});

module.exports = router;
