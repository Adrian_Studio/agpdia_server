// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const CurrencyInfoSchema = new Schema({
  symbol: { type: String },
  name: { type: String },
  symbol_native: { type: String },
  decimal_digits: { type: Number },
  rounding: { type: Number },
  code: { type: String },
  name_plural: { type: String },
});

// Schema export
module.exports = mongoose.model("Currency_info", CurrencyInfoSchema);
