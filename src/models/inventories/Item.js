// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const ItemSchema = new Schema({
  user_id: { type: String, select: false },
  name: { type: String, required: true },
  currency_id:{ type: String, required: true  },
  currency_symbol: { type: String, required: true  },
  currency_format: { type: String, required: true  },
  has_main_currency: { type: Boolean, required:true  },
  inventory_id: { type: String, required: true},
  inventory_name: { type: String, required: true },
  location_id: { type: String, required: true},
  location_name: { type: String, required: true },
  group_id: { type: String, required: true },
  group_name: { type: String, required: true },
  brand: { type: String },
  identifier: { type: String },
  acquisition_place: { type: String },
  acquisition_date: { type: Date, required: true },
  acquisition_price: { type: Number, required: true },
  dispose_date: { type: Date },
  note: { type: String },
  image: { type: String },
  invoice: { type: String },
  state:{
    type: String,
    enum: [
      "good",
      "regular",
      "bad",
      "disposed",
    ],
    default: "good"
  }
});

// Schema export
module.exports = mongoose.model("Inventories_Item", ItemSchema);
