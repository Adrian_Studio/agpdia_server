// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const AccountsAlertSchema = new Schema({
  user_id: { type: String, select: false },
  name: { type: String, select: true },
  category_id: { type: String, required: true },
  category_name: { type: String, required: true },
  concept_id: { type: String, required: true },
  concept_name: { type: String, required: true },
  periodicity_months: { type: Number, default: 1 },
  periods_to_alert: { type: Number, default: 1 },
  start_date: { type: Date, required: true },
  end_date: { type: Date, required: true },
  note: { type: String },
  is_active: { type: Boolean, default: true },
});

// Schema export
module.exports = mongoose.model(
  "Accounts_alert",
  AccountsAlertSchema
);
