// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const MessageSchema = new Schema({
  user_id: { type: String, select: false },
  alert_id: { type: String, required: true },
  section: {
    type: String,
    enum: ["accounting", "trips"],
    required: true,
  },
  message: { type: String, required: true },
  unread: { type: Boolean, default: true },
  message_date: { type: Date, required: true }, //Date of when shoud de record have been written.
  expiration_date: { type: Date, required: true },
});

// Schema export
module.exports = mongoose.model("Message", MessageSchema);
