// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const VehicleSchema = new Schema({
  user_id: { type: String, select: false },
  name: { type: String, required: true },
  power_source: {
    type: String,
    enum: ["gasoline", "electricity"],
    required: true,
  },
});

// Schema export
module.exports = mongoose.model("Vehicle", VehicleSchema);
