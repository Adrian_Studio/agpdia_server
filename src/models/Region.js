// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const RegionSchema = new Schema({
  name: { type: String },
  code: { type: String }, //Código de la provincia
  country: { type: String }, //Código del pais
});

// Schema export
module.exports = mongoose.model("Region", RegionSchema);
