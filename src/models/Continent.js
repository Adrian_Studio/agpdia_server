// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const ContinentSchema = new Schema({
  name: { type: String },
  code: { type: String },
});

// Schema export
module.exports = mongoose.model("Continent", ContinentSchema);
