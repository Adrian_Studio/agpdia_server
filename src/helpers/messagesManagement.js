const AccountsAlert = require("../models/alerts/AccountsAlert");
const Message = require("../models/alerts/Message");
const Record = require("../models/accounts/Record");
const moment = require("moment");

async function updateAllMessages() {
  deleteExpiredMessages();

  const alerts = await AccountsAlert.find().select("+user_id");
  alerts.forEach(async (alert) => {
    var teoricPeriodStart = moment().subtract(
      alert.periods_to_alert * alert.periodicity_months,
      "month"
    ); //Teoric start
    var monthsDiff = Math.ceil(moment(teoricPeriodStart).diff(
      moment(alert.start_date),
      "months",
      true
    )); // 1
    var alertPeriodStart = moment(alert.start_date)
      .add(monthsDiff + alert.periodicity_months, "months")
      .toDate();
    if (moment(alert.end_date).toDate() > alertPeriodStart && alert.is_active) {
      var period = alertPeriodStart;
      while (
        period <= moment().toDate() &&
        period <= moment(alert.end_date).toDate()
      ) {
        var nextPeriod = moment(period)
          .add(alert.periodicity_months, "month")
          .toDate();
        var existRecord = await Record.exists({
          concept_id: alert.concept_id,
          date: { $gte: period, $lte: nextPeriod },
        });
        if (!existRecord) {
          var existMessage = await Message.exists({
            alert_id: alert._id,
            message_date: period,
          });
          if (!existMessage) {
            var user_id = alert.user_id;
            var alert_id = alert._id;
            var section = "accounting";
            // var message = `Falta "${
            //   alert.name
            // }" con fecha aproximada del ${moment(period).format("L")}.`;
            var message = alert.name;
            var unread = true;
            var message_date = period;
            var expiration_date = moment(period).add(
              alert.periods_to_alert * alert.periodicity_months,
              "month"
            );
            var messageAlert = new Message({
              user_id,
              alert_id,
              section,
              message,
              unread,
              message_date,
              expiration_date,
            });
            messageAlert.save();
          }
        }
        period = nextPeriod;
      }
    }
  });
}

async function deleteExpiredMessages() {
  var filter = { expiration_date: { $lt: moment().toDate() } };
  await Message.deleteMany(filter);
}

async function updateAlertMessages(alert_id) {
  await Message.deleteMany({ alert_id });

  const alert = await AccountsAlert.findById(alert_id).select("+user_id");
  if (alert) {
    var teoricPeriodStart = moment().subtract(
      alert.periods_to_alert * alert.periodicity_months,
      "month"
    ); //Teoric start
    var monthsDiff = Math.ceil(moment(teoricPeriodStart).diff(
      moment(alert.start_date),
      "months",
      true
    )); // 1
    
    var alertPeriodStart = moment(alert.start_date)
      .add(monthsDiff + alert.periodicity_months, "months")
      .toDate();

    if (moment(alert.end_date).toDate() > alertPeriodStart && alert.is_active) {
      var period = alertPeriodStart;
      while (
        period <= moment().toDate() &&
        period <= moment(alert.end_date).toDate()
      ) {
        var nextPeriod = moment(period)
          .add(alert.periodicity_months, "month")
          .toDate();
        var existRecord = await Record.exists({
          concept_id: alert.concept_id,
          date: { $gte: period, $lte: nextPeriod },
        });
        if (!existRecord) {
          var existMessage = await Message.exists({
            alert_id: alert._id,
            message_date: period,
          });
          if (!existMessage) {
            var user_id = alert.user_id;
            var alert_id = alert._id;
            var section = "accounting";
            // var message = `Falta "${
            //   alert.name
            // }" con fecha aproximada del ${moment(period).format("L")}.`;
            var message = alert.name;
            var unread = true;
            var message_date = period;
            var expiration_date = moment(period).add(
              alert.periods_to_alert * alert.periodicity_months,
              "month"
            );
            var messageAlert = new Message({
              user_id,
              alert_id,
              section,
              message,
              unread,
              message_date,
              expiration_date,
            });
            messageAlert.save();
          }
        }
        period = nextPeriod;
      }
    }
  }
}

async function updateUserMessages(user_id) {
  const alerts = await AccountsAlert.find({user_id}).select("+user_id");
 
  alerts.forEach(async (alert) => {
    var teoricPeriodStart = moment().subtract(
      alert.periods_to_alert * alert.periodicity_months,
      "month"
    ); //Teoric start
    var monthsDiff = Math.ceil(moment(teoricPeriodStart).diff(
      moment(alert.start_date),
      "months",
      true
    )); // 1
    
    var alertPeriodStart = moment(alert.start_date)
      .add(monthsDiff + alert.periodicity_months, "months")
      .toDate();
    if (moment(alert.end_date).toDate() > alertPeriodStart && alert.is_active) {
      var period = alertPeriodStart;
      while (
        period <= moment().toDate() &&
        period <= moment(alert.end_date).toDate()
      ) {
        var nextPeriod = moment(period)
          .add(alert.periodicity_months, "month")
          .toDate();
        var existRecord = await Record.exists({
          concept_id: alert.concept_id,
          date: { $gte: period, $lte: nextPeriod },
        });
        if (!existRecord) {
          var existMessage = await Message.exists({
            alert_id: alert._id,
            message_date: period,
          });
          if (!existMessage) {
            var user_id = alert.user_id;
            var alert_id = alert._id;
            var section = "accounting";
            // var message = `Falta "${
            //   alert.name
            // }" con fecha aproximada del ${moment(period).format("L")}.`;
            var message = alert.name;
            var unread = true;
            var message_date = period;
            var expiration_date = moment(period).add(
              alert.periods_to_alert * alert.periodicity_months,
              "month"
            );
            var messageAlert = new Message({
              user_id,
              alert_id,
              section,
              message,
              unread,
              message_date,
              expiration_date,
            });
            messageAlert.save();
          }
        }else{
          var existMessage = await Message.deleteMany({
            alert_id: alert._id,
            message_date: period,
          });
        }
        period = nextPeriod;
      }
    }
  });
}

module.exports = { updateAllMessages, updateAlertMessages, updateUserMessages };
