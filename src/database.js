const mongoose = require("mongoose");
const config = require("./config/config");

mongoose
  .connect(config.dbUrl, {
    useUnifiedTopology: true,
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
  })
  .then((db) => console.log("The db is connected"))
  .catch((err) => console.log(`Error trying to conect to the db: ${err}`));
